
var app = angular.module('TestTaskApp', ['ngAnimate', 'ui.bootstrap']);

app.controller('PostsCtrl', function ($scope, $http, appConfig) {
	
	$scope.messagePost = 'Записи загружаются...';
	$scope.postClass = 'list-group-item-success';
	
	var apiUrl = appConfig.apiUrl;
    $http.get(apiUrl).
        success(function(data, status, headers, config) {
			if (data == null || data == undefined || data.length == 0){
				 $scope.postClass = 'list-group-item-warning';
				 $scope.messagePost = 'Записи отсутствуют.';	
			}
			else 
				$scope.posts = data;
        }).
        error(function(data, status, headers, config) {
			$scope.postClass = 'list-group-item-danger';
			$scope.messagePost = 'Ошибка, записи не загружены. Попробуйте снова.';
        });
});

app.controller('CommentsCtrl', function ($scope, $http, appConfig) {
	
	$scope.messageComment = 'Комментарии загружаются...';
	$scope.commentClass = 'list-group-item-success';
	
    var apiUrl = appConfig.apiUrl;
    apiUrl = apiUrl.concat($scope.post.id, '/comments');
    $http.get(apiUrl).
        success(function(data, status, headers, config) {
			if (data == null || data == undefined || data.length == 0){
				$scope.commentClass = 'disabled';
				$scope.messageComment = 'Нет комментариев к данной записи.';	
			}
			else
				$scope.comments = data;
        }).
        error(function(data, status, headers, config) {
			$scope.commentClass = 'list-group-item-danger';
			$scope.messageComment = 'Ошибка, комментарии не загружены. Попробуйте снова.';
        });
});